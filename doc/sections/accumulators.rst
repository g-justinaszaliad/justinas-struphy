.. _accums:

Accumulators
============

Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.pic.particles_to_grid
    struphy.pic.accum_kernels
    struphy.pic.accum_kernels_gc

.. toctree::
    :caption: Lists of available accumulators:

    STUBDIR/struphy.pic.particles_to_grid
    STUBDIR/struphy.pic.accum_kernels
    STUBDIR/struphy.pic.accum_kernels_gc


.. _accumulator:

Base classes
------------

.. automodule:: struphy.pic.particles_to_grid
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance:


Kernels
-------

.. automodule:: struphy.pic.accum_kernels
    :members:
    :undoc-members:
    :exclude-members: variables
    :show-inheritance:

