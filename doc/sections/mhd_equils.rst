.. _mhd_equil:

MHD equilibria
==============

Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.fields_background.mhd_equil.base
    struphy.fields_background.mhd_equil.equils

.. toctree::
    :caption: Lists of available MHD equilibria:

    STUBDIR/struphy.fields_background.mhd_equil.base
    STUBDIR/struphy.fields_background.mhd_equil.equils

    
.. _mhd_base:

Base classes
------------

.. automodule:: struphy.fields_background.mhd_equil.base
    :members:
    :undoc-members: 
    :exclude-members: 
    :show-inheritance:


.. _mhd_equil_avail:

Available MHD equilibria
------------------------

.. automodule:: struphy.fields_background.mhd_equil.equils
    :members:
    :undoc-members:
    :show-inheritance:





