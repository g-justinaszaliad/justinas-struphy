grid :
    Nel      : [16, 3, 3] # number of grid cells, >p
    p        : [2, 2, 2]  # spline degree
    spl_kind : [True, True, True] # spline type: True=periodic, False=clamped
    bc       : [[null, null], [null, null], [null, null]] # boundary conditions for N-splines (homogeneous Dirichlet='d')
    dims_mask : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el    : [6, 2, 2] # quadrature points per grid cell
    nq_pr    : [6, 2, 2] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

units : # units not stated here can be viewed via "struphy units -h"
    x      : 0.022771076724847367 # Om_c * v_A
    B      : 1. # magnetic field unit in T
    n      : 1. # number density unit in 10^20 m^(-3)

time :
    dt         : 0.15  # time step
    Tend       : 4.5 # simulation time interval is [0, Tend]
    split_algo : Strang # LieTrotter | Strang

geometry :
    type : Cuboid # mapping F (possible types seen below)
    Cuboid :
        l1 : 0. # start of interval in eta1
        r1 : 7.853981633974483 # 2*pi/0.8
        l2 : 0. # start of interval in eta2
        r2 : 1. # end of interval in eta2, r2>l2
        l3 : 0. # start of interval in eta3
        r3 : 1. # end of interval in eta3, r3>l3 

mhd_equilibrium :
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        B0x  : 1. # magnetic field in x
        B0y  : 0. # magnetic field in y
        B0z  : 0. # magnetic field in z
        beta : 0.1 # plasma beta = 2*p*mu_0/B^2
        n0   : 1. # number density

em_fields :
    init :
        type : ModesSin # type of initialization
        ModesSin :
            coords : 'physical' # in which coordinates (logical or physical)
            comps :
                b2 : [False, False, True] # components to be initialized (for scalar fields: no list)
            ls : [1] # Integer mode numbers in x or eta_1 (depending on coords)
            ms : [0] # Integer mode numbers in y or eta_2 (depending on coords)
            ns : [0] # Integer mode numbers in z or eta_3 (depending on coords)
            amps : [0.001] # amplitudes of each mode
            Lx : 7.853981633974483 # 2*pi/0.8
            Ly : 1.
            Lz : 1.

fluid :
    mhd :
        phys_params:
            A : 1  # mass number in units of proton mass
            Z : 1 # signed charge number in units of elementary charge
        mhd_u_space : H1vec # Hdiv | H1vec
        init :
            type : null # type of initialization

kinetic :
    energetic_ions :
        phys_params:
            A : 4  # mass number in units of proton mass
            Z : 2 # signed charge number in units of elementary charge
        markers :
            type    : full_f # full_f, control_variate, or delta_f
            ppc     : 200 # alternative if ppc = null (total number of markers, must be larger or equal than # MPI processes)
            eps     : .25 # MPI send/receive buffer (0.1 <= eps <= 1.0)
            bc_type : [periodic, periodic, periodic] # marker boundary conditions: remove, reflect or periodic
            loading :
                type    : pseudo_random # particle loading mechanism
                seed    : null # seed for random number generator
                moments : [2.5, 0., 0., 1., 1., 1.] # moments of Gaussian s3, see background/moms_spec
                spatial : disc # uniform or disc
        init :
            type : Maxwellian6DUniform
            Maxwellian6DUniform :
                n : 0.05
                u1 : 2.5
        save_data :
            n_markers : 200 # number of markers to be save during simulation
            f :
                slices : [v1, e1_v1] # in which directions to bin (e.g. [e1_e2, v1_v2_v3])
                n_bins : [[32], [32, 32]] # number of bins in each direction (e.g. [[16, 20], [16, 18, 22]])
                ranges : [[[-5.5, 5.5]], [[0., 1.], [-5.5, 5.5]]] # bin range in each direction
        push_algos :
            vxb : analytic # possible choices: analytic, implicit
            eta : rk4 # possible choices: forward_euler, heun2, rk2, heun3, rk4

solvers :
    solver_1 :
        type : PBiConjugateGradientStab
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-14
        maxiter : 3000
        info : False
        verbose : False
    solver_2 :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-14
        maxiter : 3000
        info : False
        verbose : False
    solver_3 :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-14
        maxiter : 3000
        info : False
        verbose : False
    solver_4 :
        type : PBiConjugateGradientStab
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-14
        maxiter : 3000
        info : False
        verbose : False
