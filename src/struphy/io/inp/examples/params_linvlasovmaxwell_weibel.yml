# The Weibel instability with the following initial distribution function
# 
#     f(t=0,x,v) = 1 / [(2*pi)^(3/2) * sigma1 * sigma2 * sigma3] \
#                   * exp(- v1^2/(2 * sigma1^2) - v2^2/(2 * sigma2^2) - v3^2/(2 * sigma3^2)) \
#                   * [1 + alpha * cos(k*x)]
# 
# and magnetic field in z-direction
# 
#     B_3(t=0,x) = beta * cos(k*x)
# 
# with sigma1 = 0.02/sqrt{2}, sigma2 = sqrt{12}*sigma1, sigma3 = sigma1, k = (1.25,0,0)^T,
# alpha=0, beta = -10^{-4}

grid :
    Nel       : [32, 2, 2] # number of grid cells, >p
    p         : [3, 1, 1]  # spline degree
    spl_kind  : [True, True, True] # spline type: True=periodic, False=clamped
    bc        : [[null, null], [null, null], [null, null]] # boundary conditions for N-splines (homogeneous Dirichlet='d')
    dims_mask : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el     : [2, 2, 2] # quadrature points per grid cell
    nq_pr     : [2, 2, 2] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck  : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

units :
    x   : 0.0017045090240267625 # c * m_e / e
    B   : 1. # magnetic field unit in T
    n   : 0.09719853837468743 # eps_0 / m_e / 1e20 # bulk number density unit in 1 x 10^20 m^(-3)

time :
    dt         : 0.05 # time step
    Tend       : 100.0 # simulation time interval is [0, Tend]
    split_algo : LieTrotter # LieTrotter | Strang

geometry :
    type : Cuboid # mapping F (possible types seen below)
    Cuboid :
        l1       : 0. 
        r1       : 5.026548245743669 # 2*pi / k_1
        l2       : 0.
        r2       : 1.
        l3       : 0.
        r3       : 1.

mhd_equilibrium :
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        B0x  : 0. # magnetic field in x
        B0y  : 0. # magnetic field in y
        B0z  : 0. # magnetic field in z
        beta : 0. # plasma beta = 2*p*mu_0/B^2
        n0   : 1. # number density

electric_equilibrium :
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        phi0  : 1. # constant electric potential

em_fields :
    init :
        type : ModesCos # initial conditions (possible types seen below)
        ModesCos :
            coords : 'physical' # in which coordinates (logical or physical)
            comps :
                e_field : [False, False, False]  # components to be initialized (for scalar fields: no list)
                b_field : [False, False, True] # components to be initialized (for scalar fields: no list)
            ls : [1.]
            ms : [0]
            ns : [0.]
            amp : [-0.0001] # amplitudes of each mode
            Lx : 5.026548245743669

kinetic :
    electrons :
        phys_params :
            A : 0.0005446170214876324  # electron mass number in units of proton mass
            Z : -1 # signed charge number in units of elementary charge
        markers :
            type    : delta_f # full_f, control_variate, or delta_f
            Np      : 100000 # alternative if ppc not given (total number of markers, Np must be >= # MPI processes)
            eps     : 0.2 # MPI send/receive buffer (0.1 <= eps <= 1.0)
            bc_type : [periodic, periodic, periodic] # remove, reflect or periodic
            loading :
                type    : pseudo_random # particle loading mechanism
                seed    : 1234 # seed for random number generator
                moments : [0., 0., 0., 0.014, 0.049, 0.014] # sample after f1
                spatial : uniform # uniform or disc
        init :
            type : Maxwellian6DPerturbed
            Maxwellian6DPerturbed :
                n :
                    n0 : 0 # here 0 because we want to initialize f1
                    perturbation :
                        l : [1.]
                        m : [0.]
                        n : [0.]
                        amps_sin : [0.]
                        amps_cos : [-0.0001] # alpha
                u1 :
                    u01 : 0.
                u2 :
                    u02 : 0.
                u3 :
                    u03 : 0.
                vth1 :
                    vth01 : 0.014142135623730949
                vth2 :
                    vth02 : 0.04898979485566356
                vth3 :
                    vth03 : 0.014142135623730949
        background :
            type : Maxwellian6DUniform
            Maxwellian6DUniform :
                n  : 1.
                u1 : 0.
                u2 : 0.
                u3 : 0.
                vth1 : 0.014142135623730949
                vth2 : 0.04898979485566356
                vth3 : 0.014142135623730949
        save_data :
            n_markers : 1 # number of markers to be saved during simulation
            f :
                slices : [e1, v1_v2] # in which directions to bin (e.g. [e1_e2, v1_v2_v3])
                n_bins : [[32], [64, 64]] # number of bins in each direction (e.g. [[16, 20], [16, 18, 22]])
                ranges : [[[0., 1.]], [[-0.1, 0.1], [-0.1, 0.1]]] # bin range in each direction (e.g. [[[0., 1.], [0., 1.]], [[-3., 3.], [-4., 4.], [-5., 5.]]])
        push_algos :
            vxb : analytic # possible choices: analytic, implicit
            eta : rk4 # possible choices: forward_euler, heun2, rk2, heun3, rk4

solvers :
    solver_poisson :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False
    solver_ew :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False
    solver_eb :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False
